const mysql = require('../configs/mysql.js');

class Dokumen {

	static save(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO dokumen(nama, keterangan, file, type, jenis, grup_id, skpd_id)
				VALUES('${data.nama}', '${data.keterangan}', '${data.file}', '${data.type}', '${data.jenis}', '${data.grup_id}', '${data.skpd_id}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					dokumen.id,
					DATE_FORMAT(dokumen.tgl, '%d %M %Y %H:%i:%s') AS tgl,
					dokumen.nama,
					dokumen.keterangan,
					dokumen.type,
					dokumen.file,
					grup.nama AS grup_nama,
					skpd.nama AS skpd_nama
				FROM dokumen
				LEFT JOIN grup ON dokumen.grup_id = grup.id
				LEFT JOIN skpd ON dokumen.skpd_id = skpd.id
				ORDER BY dokumen.id DESC
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findLimit6Publik() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					dokumen.id,
					DATE_FORMAT(dokumen.tgl, '%d %M %Y %H:%i:%s') AS tgl,
					dokumen.nama,
					dokumen.keterangan,
					dokumen.type,
					dokumen.file,
					grup.nama AS grup_nama,
					skpd.nama AS skpd_nama
				FROM dokumen
				LEFT JOIN grup ON dokumen.grup_id = grup.id
				LEFT JOIN skpd ON dokumen.skpd_id = skpd.id
				WHERE dokumen.jenis = '1'
				ORDER BY dokumen.id DESC LIMIT 6
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Dokumen;
