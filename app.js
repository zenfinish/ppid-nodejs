const express = require('express');
const cors = require('cors');

require('dotenv').config();

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const adminRoute = require('./routes/admin.js');
const peopleRoute = require('./routes/people.js');
const contentRoute = require('./routes/content.js');

app.use('/admin', adminRoute);
app.use('/people', peopleRoute);
app.use('/content', contentRoute);

app.use('/', function(req, res) {
	res.json(`!@#$%%^&*`);
});

app.listen(3001, function() {
	console.log(`Running on port 3001`);
});