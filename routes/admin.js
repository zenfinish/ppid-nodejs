const express = require('express');
const AdminController = require('../controllers/admin.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.get('/cek-token', isUser, AdminController.getCekToken);
router.post('/login', AdminController.postLogin);
router.get('/transaction', isUser, AdminController.getTransaction);
router.get('/skpd', isUser, AdminController.getSkpd);
router.get('/grup', isUser, AdminController.getGrup);

router.get('/dokumen', isUser, AdminController.getDokumen);
router.post('/dokumen', isUser, AdminController.postDokumen);

module.exports = router;