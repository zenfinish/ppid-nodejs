const jwt = require('jsonwebtoken');

require('dotenv').config();

module.exports = {

	createToken(data, callback) {
		jwt.sign(data, process.env.KEY_JSONWEBTOKEN, function(err, token) {
			if (err) {
				callback(err);
			} else {
				callback(null, token);
			}
		});
	},

	verifyToken(token, callback) {
		jwt.verify(token, process.env.KEY_JSONWEBTOKEN, function(err, decoded) {
			if (err) {
				callback(err);
			} else {
				callback(null, decoded);
			}
		});
	}

}