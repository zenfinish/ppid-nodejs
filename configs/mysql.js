const mysql = require('mysql');

const connMysql = mysql.createConnection({
   host: process.env.HOST_MYSQL,
   user: process.env.USER_MYSQL,
   password: process.env.PASS_MYSQL,
   database: process.env.DB_MYSQL,
});
connMysql.connect();

module.exports = connMysql;
