const mysql = require('../configs/mysql.js');

class Transaction {

	static find(data) {
		return new Promise((resolve, reject) => {
			let batas = '';
			let batas2 = '';
			if (data.role_id !== '') batas = `&& transaction.role_id = ${data.role_id}`;
			if (data.user_id !== '') batas2 = `&& transaction.user_id = ${data.user_id}`;
			mysql.query(`
				SELECT
					DATE_FORMAT(transaction.tgl, '%d %M %Y %H:%i:%s') AS tgl,
					transaction.voucher,
					transaction.username,
					FORMAT(transaction.price, 0) AS price_format,
					transaction.price,
					role.nama AS role_name,
					user.name AS user_name
				FROM transaction
				LEFT JOIN role ON transaction.role_id = role.id
				LEFT JOIN user ON transaction.user_id = user.id
				WHERE (transaction.tgl BETWEEN '${data.date1}' AND '${data.date2} 23:59:59') ${batas} ${batas2}
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Transaction;
