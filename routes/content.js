const express = require('express');
const ContentController = require('../controllers/content.js');
const router = express.Router();

router.get('/dokumen/limit6', ContentController.getDokumenLimit6);

module.exports = router;