const Dokumen = require('../models/dokumen.js');

class PeopleController {

	static getDokumenLimit6(req, res) {
		Dokumen.findLimit6Publik()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			console.log(error)
			res.status(500).json(error);
		});
	}

}

module.exports = PeopleController;
