const express = require('express');
const PeopleController = require('../controllers/people.js');
const { isUser } = require('../middlewares/is-user.js');
const router = express.Router();

router.post('/login', PeopleController.postLogin);
router.post('/register', PeopleController.postRegister);
router.get('/aktivasi/:token', PeopleController.getAktivasi);
router.get('/cek-token', isUser, PeopleController.getCekToken);

module.exports = router;