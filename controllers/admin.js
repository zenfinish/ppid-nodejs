const Transaction = require('../models/transaction.js');
const Skpd = require('../models/skpd.js');
const Grup = require('../models/grup.js');
const User = require('../models/user.js');
const Dokumen = require('../models/dokumen.js');
const { comparePassword } = require('../configs/bcrypt.js');
const token = require('../configs/jsonwebtoken.js');
const path = require("path");
const multer = require("multer");

const storage = multer.diskStorage({
   destination: "./public/dokumen/",
   filename: function(req, file, cb){
      cb(null, Date.now() + path.extname(file.originalname));
   }
});

const upload = multer({
	storage: storage,
	limits: { fileSize: 2000000 },
}).single("file");

class AdminController {

	static getCekToken(req, res) {
		res.status(200).json('Berhasil');
	}

	static postLogin(req, res) {
		User.findOne(req.body.email)
		.then(function(result) {
			if (result) {
				let compare = comparePassword(req.body.password, result.password);
				if (compare) {
					delete result.password;
					// result2 = result.toJSON();
					token.createToken({ ...result }, function(error, token) {
						if (error) {
							res.status(500).json(error);
						} else {
							res.status(200).json(token);
						}
					});
				} else {
					res.status(500).json('Password Salah');
				}
			} else {
				res.status(500).json('Email Tidak Ditemukan');
			}
		})
		.catch(function(error) {
			console.log(error)
			res.status(500).json(error);
		})
	}

	static getTransaction(req, res) {
		Transaction.find(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getSkpd(req, res) {
		Skpd.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getGrup(req, res) {
		Grup.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static postDokumen(req, res) {
		upload(req, res, (err) => {
			if (err) {
				res.status(500).json(err);
			} else {
				req.body['file'] = req.file.filename;
				Dokumen.save(req.body)
				.then(function(result) {
					res.status(200).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error.message);
				})
			}
		});
	}

	static getDokumen(req, res) {
		Dokumen.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

}

module.exports = AdminController;
