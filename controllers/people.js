const People = require('../models/people.js');
const { hashingPassword, comparePassword } = require('../configs/bcrypt.js');
const token = require('../configs/jsonwebtoken.js');
const { sendMail } = require('../configs/nodemailer.js');

class PeopleController {

	static getCekToken(req, res) {
		res.status(200).json('Berhasil');
	}
	
	static postLogin(req, res) {
		People.findOne(req.body.email)
		.then(function(result) {
			if (result) {
				let compare = comparePassword(req.body.password, result.password);
				if (compare) {
					delete result.password;
					token.createToken({ ...result }, function(error, token) {
						if (error) {
							res.status(500).json(error);
						} else {
							res.status(200).json(token);
						}
					});
				} else {
					res.status(500).json('Password Salah');
				}
			} else {
				res.status(500).json('Email Tidak Ditemukan');
			}
		})
		.catch(function(error) {
			console.log(error)
			res.status(500).json(error);
		});
	}

	static postRegister(req, res) {
		People.findOne(req.body.email)
		.then(function(result) {
			if (result) {
				res.status(500).json('Email Sudah Ada.');
			} else {
				token.createToken(req.body, function(error, token) {
					if (error) {
						res.status(500).json(error);
					} else {
						let mailOptions = {
							from: process.env.EMAIL_NODEMAILER,
							to: 'admin@bandarlampungkota.go.id',
							subject: 'Aktivasi Akun PPID Kota Bandar Lampung',
							html: `
								<h1>Selamat Datang di PPID Kota Bandar Lampung</h1>
								<p>Anda telah melakukan pendaftaran sebagai pemohon informasi di PPID. Saat ini akun Anda belum dapat digunakan untuk mengajukan permohonan.</p>
								<p>Untuk dapat melakukan permohonan informasi, Anda harus melakukan aktivasi dengan mengikuti tautan yang kami kirim dibawah ini.</p>
								<button><a href="http://localhost:3000/#/aktivasi/${token}" target="_blank">Aktifkan Akun Anda Disini</a></button>
								<p>Terima kasih. <u>Tim PPID Kota Bandar Lampung</u></p>
							`,
						};
						sendMail(mailOptions)
						.then(function(result) {
							res.status(200).json(result);
						})
						.catch(function(error) {
							res.status(500).json(error.message);
						})
					}
				});
			}
		})
		.catch(function(error) {
			console.log(error)
			res.status(500).json(error);
		})
	}

	static getAktivasi(req, res) {
		token.verifyToken(req.params.token, function(error, decoded) {
			if (error) {
				res.status(500).json(error);
			} else {
				decoded['password'] = hashingPassword(decoded.password);
				People.save(decoded)
				.then(function(result) {
					res.status(200).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			}
		});
	}

}

module.exports = PeopleController;
