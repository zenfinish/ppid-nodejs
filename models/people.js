const mysql = require('../configs/mysql.js');

class People {

	static findOne(email) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM people
				WHERE email = '${email}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static save(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO people(email, password, nama, nik, alamat, hp, propinsi, kota)
				VALUES('${data.email}', '${data.password}', '${data.nama}', '${data.nik}', '${data.alamat}', '${data.hp}', '${data.propinsi}', '${data.kota}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = People;
