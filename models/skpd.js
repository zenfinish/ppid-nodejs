const mysql = require('../configs/mysql.js');

class Skpd {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM skpd
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Skpd;
