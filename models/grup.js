const mysql = require('../configs/mysql.js');

class Grup {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM grup
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Grup;
